package si.uni_lj.fri.pbd.miniapp2;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MediaPlayerService extends Service {

    private static final String TAG = MediaPlayerService.class.getSimpleName();

    static AccelerationService accService;
    static boolean serviceBound;
    static boolean serviceStarted;
    private static MediaPlayer player;
    private static AssetFileDescriptor afd;

    //if play is pressed for the first time
    public static boolean pressed;
    //Current time of the track playing
    private static double startTime = 0;
    //Final time of the track playing
    private static double finalTime = 0;

    //Current time of the track playing
    private static double startTimeN = 0;
    //Final time of the track playing
    private static double finalTimeN = 0;

    private static AssetManager am;
    //Array for storing mpr3 file names
    private static String[] songNames;
    //State for defining if the player is paused(true) or stopped(false)
    private static boolean state;


    //postition of the song in the songNames array
    private static int songNumber;
    public static Handler myHandler = new Handler();
    public static Handler notifyHandler = new Handler();
    public static NotificationManager nm;
    //Defining serviceBinder and instantiating it to RunServiceBinder
    private final IBinder serviceBinder = new RunServiceBinder();
    public static Context context;
    //Actions and ChannelID
    public static final String ACTION_STOP = "stop_service_stop_music";

    public static final String ACTION_PLAY = "start_service_play_music";
    public static final String ACTION_EXIT = "exit_service";

    private static final String channelID = "MusicPlayer";
    private static boolean stopped;


    //Defining a static Notification ID


    static final int NOTIFICATION_ID = 100;

    //Connection for connecting to the Acceleration service
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "Service bound");
            AccelerationService.RunServiceBinder binder = (AccelerationService.RunServiceBinder) iBinder;
            serviceBound = true;
            accService = binder.getService();

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "Service disconnect");
            serviceBound = false;
        }
    };

    @Override
    public void onCreate() {
        Log.d(TAG, "Creating service");

        // Defining common notification manager
        nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        am = getAssets();
        player = new MediaPlayer();
        state = false;
        pressed = false;
        stopped = false;
        //accelerations service not bound
        serviceBound=false;
        try {
            //Initializing the list of the songs(sounds) in the asset folder
            songNames = am.list("");


        } catch (IOException e) {
            e.printStackTrace();
        }

        //creating notification channel
        createNotificationChannel();

    }



    //Returning serviceBinder
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return this.serviceBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int  flags, int startId) {
        //if acceleration servie is not bound yet, bound it
        if(!serviceBound) {
            Intent intentAcc = new Intent(MediaPlayerService.this, AccelerationService.class);
            bindService(intentAcc, mConnection, Context.BIND_AUTO_CREATE);
        }

        //Starting the service on PLAY command
        Log.d(TAG, "Starting service");
        if (MainActivity.actionPlay) {
            MainActivity.actionPlay = false;
            startMusic(getApplicationContext());
        }

        //When a song is over on the player, it chooses another random song from the song list and plays it
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                player.reset();
                state = false;
                Log.d(TAG, "Playing next song");
                startMusic(getApplicationContext());

            }
        });

        // pressing STOP in the notification
        if (intent.getAction() == ACTION_STOP && state) {
            Log.d(TAG, "Stoping music");
            Toast.makeText(getApplicationContext(), "The player is stopped.", Toast.LENGTH_LONG).show();
            stopMusic(getApplicationContext());
        }
        // Pressing STOP in the notification when the player is already stopped
        else if (intent.getAction() == ACTION_STOP && !state) {
            Toast.makeText(getApplicationContext(), "The player is already stopped.", Toast.LENGTH_LONG).show();

            //exiting the app
        } else if (intent.getAction() == ACTION_EXIT) {
            if (player.isPlaying()) {
                stopMusic(getApplicationContext());
            }
            Toast.makeText(getApplicationContext(), "Exiting the app", Toast.LENGTH_LONG).show();
            if(serviceStarted && serviceBound){
                stopAccService(getApplicationContext());
                unbindService(mConnection);
            }
            stopForeground(true);
            stopSelf();
            System.exit(0);
        }

        //Pressing PLAY in the notification
        if (intent.getAction() == ACTION_PLAY) {
            //if player is already playing then pause the music
            if (player.isPlaying()) {
                Log.d(TAG, "Pausing music");
                player.pause();
                state = true;
                Toast.makeText(getApplicationContext(), "The player is paused.", Toast.LENGTH_LONG).show();
            }
            //if player was stopped then start playing a new random song
            else {
                Log.d(TAG, "Starting music");
                Toast.makeText(getApplicationContext(), "The player is starting.", Toast.LENGTH_LONG).show();
                startMusic(this);
            }


        }

        return START_STICKY;
    }

    //create notification
    private Notification createNotification(String name, Context ctx, String time) {
        Intent notificationIntent = new Intent(ctx, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(ctx,
                0, notificationIntent, 0);

        Intent actionIntent = new Intent(ctx, MediaPlayerService.class);
        actionIntent.setAction(ACTION_PLAY);
        PendingIntent playPendingIntent = PendingIntent.getService(ctx, 0,
                actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent stopIntent = new Intent(ctx, MediaPlayerService.class);
        stopIntent.setAction(ACTION_STOP);
        PendingIntent stopPendingIntent = PendingIntent.getService(ctx, 0,
                stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent exitIntent = new Intent(ctx, MediaPlayerService.class);
        exitIntent.setAction(ACTION_EXIT);
        PendingIntent exitPendingIntent = PendingIntent.getService(ctx, 0,
                exitIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        MediaSessionCompat mediaSession = new MediaSessionCompat(ctx, "tag");
        Notification notification = new NotificationCompat.Builder(ctx, channelID)
                .setContentTitle(name)
                .setContentText(time)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.note)
                .setShowWhen(false)
                .addAction(R.drawable.play, "Play", playPendingIntent)
                .addAction(R.drawable.stop, "Stop", stopPendingIntent)
                .addAction(R.drawable.exit, "Exit", exitPendingIntent)
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(0, 1, 2)
                        .setMediaSession(mediaSession.getSessionToken()))

                //.setSmallIcon(R.drawable.ic_stat_name)
                .setContentIntent(pendingIntent)
                .build();
        return notification;
    }

    //Creating notification channel for the foreground service
    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT < 26) {
            return;
        } else {

            NotificationChannel channel = new NotificationChannel(MediaPlayerService.channelID,
                    "Foreground Service Channel", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription("Posts notification related to the media player service");
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);

            NotificationManager managerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            managerCompat.createNotificationChannel(channel);
        }
    }

    //start the music and opreapre the player
    public void startMusic(Context ctx) {
        MainActivity.actionPlay = false;
        context = ctx;
        try {
            if (state) {
                player.start();
            } else {
                player.reset();
                songNumber = playRandomSound(songNames);
                afd = ctx.getAssets().openFd(songNames[songNumber]);
                player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
                afd.close();
                player.prepare();
                /*player.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){

                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                    }
                });*/
                player.start();
                //upgrade the UI and notification
                stopped = false;
                updateNotificationOnPlay(ctx);
                updateUIOnPlay(ctx);


            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //pause the music
    public static void pauseMusic() {
        state = true;
        player.pause();
    }
    //stop the music
    public void stopMusic(Context c) {
        state = false;
        player.stop();
        stopped = true;
        updateNotificationOnStop(c);
        updateUIOnStop();

    }

    //method for stoping the foreground process
    public void background() {
        stopForeground(true);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(serviceBound) {
            serviceBound=false;
            unbindService(mConnection);
        }
        //stopping the player when service is destroyed
        player.stop();
    }
    // update notification every second
    private Runnable UpdateSongTime = new Runnable() {
        public void run() {

            startTime = player.getCurrentPosition();
            MainActivity.finish.setText(String.format("%d min : %d sec / %d min : %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTime)), TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                    finalTime)))
            );


            myHandler.postDelayed(this, 100);

        }
    };
    //update notification
    private Runnable UpdateNotification = new Runnable() {

        public void run() {
            if (!stopped) {
                startTimeN = player.getCurrentPosition();
                nm.notify(NOTIFICATION_ID, createNotification(songNames[songNumber], context,
                        String.format("%d min : %d sec / %d min : %d sec",
                                TimeUnit.MILLISECONDS.toMinutes((long) startTimeN),
                                TimeUnit.MILLISECONDS.toSeconds((long) startTimeN) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                                toMinutes((long) startTimeN)), TimeUnit.MILLISECONDS.toMinutes((long) finalTimeN),
                                TimeUnit.MILLISECONDS.toSeconds((long) finalTimeN) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                                finalTimeN)))));

                notifyHandler.postDelayed(this, 100);
            }
        }
    };
  //update UI
    public void updateUIOnPlay(Context ctx) {
        MainActivity.start.setVisibility(View.INVISIBLE);
        MainActivity.songInfo.setText(songNames[songNumber]);
        MainActivity.finish.setVisibility(View.VISIBLE);
        finalTime = player.getDuration();
        startTime = player.getCurrentPosition();
        MainActivity.finish.setText(String.format("%d min : %d sec / %d min : %d sec",
                TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                startTime)),
                TimeUnit.MILLISECONDS.toMinutes((long) finalTime),
                TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                finalTime))));


        myHandler.postDelayed(UpdateSongTime, 100);


    }
    //update notification on play
    public void updateNotificationOnPlay(Context ctx) {
        finalTimeN = player.getDuration();
        startTimeN = player.getCurrentPosition();
        if (!pressed) {
            pressed = true;

            startForeground(NOTIFICATION_ID, createNotification(songNames[songNumber], ctx,
                    String.format("%d min : %d sec / %d min : %d sec",
                            TimeUnit.MILLISECONDS.toMinutes((long) startTimeN),
                            TimeUnit.MILLISECONDS.toSeconds((long) startTimeN) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            startTimeN)),
                            TimeUnit.MILLISECONDS.toMinutes((long) finalTimeN),
                            TimeUnit.MILLISECONDS.toSeconds((long) finalTimeN) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            finalTimeN)))));
        } else {

            nm.notify(NOTIFICATION_ID, createNotification(songNames[songNumber], ctx,
                    String.format("%d min : %d sec / %d min : %d sec",
                            TimeUnit.MILLISECONDS.toMinutes((long) startTimeN),
                            TimeUnit.MILLISECONDS.toSeconds((long) startTimeN) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            startTimeN)),
                            TimeUnit.MILLISECONDS.toMinutes((long) finalTimeN),
                            TimeUnit.MILLISECONDS.toSeconds((long) finalTimeN) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                            finalTimeN)))));
        }
        notifyHandler.postDelayed(UpdateNotification, 100);


    }
   //update notification on stop
    public void updateNotificationOnStop(Context c) {
        nm.notify(NOTIFICATION_ID, createNotification("No song is playing", c, "Duration"));
    }

    public static void updateUIOnStop() {
        MainActivity.finish.setVisibility(View.INVISIBLE);
        MainActivity.songInfo.setText("No song is playing");
        MainActivity.start.setVisibility(View.VISIBLE);
    }

    //Choose a random song from the song list, if randomInt==2 then song is inbuilt file images (not mp3 file)
    private int playRandomSound(String soundList[]) {
        int randomInt = (new Random().nextInt(soundList.length - 1));
        while (randomInt == 2) {
            randomInt = (new Random().nextInt(soundList.length - 1));
        }
        return randomInt;
    }


    //Internal Service Binder Class
    public class RunServiceBinder extends Binder {
        MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }


    //starting the acceleration sercvice
    public void startAccService(Context c)
    {

        if(serviceBound) {

            accService.startAcc();
            Toast.makeText(getApplication(), "Gesture commands enabled",
                    Toast.LENGTH_LONG).show();
            Log.d(TAG, "Started service");

        }
        else{
            Toast.makeText(getApplication(), "Acceleration service not bound",
                    Toast.LENGTH_LONG).show();

        }
    }
    //stopping acceleration service
    public void stopAccService(Context c)
    {
        if(serviceBound) {
         accService.stopAcc();
            Toast.makeText(getApplication(), "Gesture commands disabled",
                    Toast.LENGTH_LONG).show();
            Log.d(TAG, "Stopped service");

        }

        else{
            Toast.makeText(getApplication(), "Gesture commands already disabled",
                    Toast.LENGTH_LONG).show();

        }
    }
}