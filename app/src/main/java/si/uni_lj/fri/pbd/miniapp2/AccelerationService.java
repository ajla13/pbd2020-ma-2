package si.uni_lj.fri.pbd.miniapp2;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class AccelerationService extends Service implements SensorEventListener {

    private final IBinder serviceBinder=new RunServiceBinder();

    private  SensorManager mSensorManager;
    private  Sensor mAccelerometer;
    private final int noise_treshold=5;
    private float Xt=0;
    private float Yt=0;
    private float Zt=0;
    private static String command;
    public static Intent i;
    public  static boolean started;
    public  static boolean stopped;
    MediaPlayerService playerService=new MediaPlayerService();

    //return binder to media player
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return this.serviceBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        started=false;
        stopped=false;
        //define sensor manager
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if(mSensorManager!= null)
        {
            mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }
        if(mAccelerometer!=null)
        {
            mSensorManager.registerListener(this,mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        }

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float X=event.values[0];
        float Y=event.values[1];
        float Z=event.values[2];

        float dX=Math.abs(X-Xt);
        float dY=Math.abs(Y-Yt);
        float dZ=Math.abs(Z-Zt);

        Xt=X;
        Yt=Y;
        Zt=Z;

        if(dX<=noise_treshold){
            dX=0;
        }
        if(dZ<=noise_treshold){
            dZ=0;
        }

        if(dY<=noise_treshold){
            dY=0;
        }

        command="IDLE";
        if(dX>dZ && started){
            command="HORIZONTAL";
            playerService.pauseMusic();
        }
        else  {
            if(started && dZ>dX) {
                command = "VERTICAL";
                playerService.startMusic(getApplication());

            }
        }

    }
    //start service
    public void startAcc(){

        started=true;
    }
    //stop service
    public void stopAcc(){

       started=false;
    }
    public void background() {
        stopForeground(true);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    public class RunServiceBinder extends Binder {
       AccelerationService getService() {
            return AccelerationService.this;
        }
    }
}
