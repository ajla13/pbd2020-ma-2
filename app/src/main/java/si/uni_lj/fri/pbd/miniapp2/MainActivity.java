package si.uni_lj.fri.pbd.miniapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import static si.uni_lj.fri.pbd.miniapp2.MediaPlayerService.ACTION_PLAY;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //button objects
    private Button buttonPlay;
    private Button buttonStop;
    private Button buttonPause;
    private Button buttonExit;
    private Button buttonGon;
    private Button buttonGoff;
    public static TextView start;
    public static TextView finish;
    public static TextView songInfo;
    public static boolean serviceStartedM;

    public  static Intent i;

    private static final String TAG = MainActivity.class.getSimpleName();
    public static boolean actionPlay=false;
    // Creatig mediaPlayer Service and serviceBound\
    MediaPlayerService playerService=new MediaPlayerService();
   static boolean  serviceBoundM;


    // Defining the service connection
    private ServiceConnection mConnectionM = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "Service bound");

            MediaPlayerService.RunServiceBinder binder = (MediaPlayerService.RunServiceBinder) iBinder;
            playerService = binder.getService();
           ;
            serviceBoundM = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "Service disconnect");
            serviceBoundM = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        i=new Intent(this,MediaPlayerService.class);
        //bind media player service
        bindService(i,mConnectionM,0);
        serviceStartedM=false;
        start=(TextView) findViewById(R.id.startS);
        finish=(TextView) findViewById(R.id.finishF);
        finish.setVisibility(View.INVISIBLE);
        songInfo=(TextView) findViewById(R.id.Info);
        //getting buttons from xml
        buttonPlay = (Button) findViewById(R.id.play);
        buttonStop = (Button) findViewById(R.id.stop);
        buttonPause = (Button) findViewById(R.id.pause);
        buttonExit = (Button) findViewById(R.id.exit);
        buttonGon = (Button) findViewById(R.id.on);
        buttonGoff = (Button) findViewById(R.id.off);

        //attaching onclicklistener to buttons
        buttonPlay.setOnClickListener(this);
        buttonStop.setOnClickListener(this);
        buttonPause.setOnClickListener(this);
        buttonExit.setOnClickListener(this);
        buttonGoff.setOnClickListener(this);
        buttonGon.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }
    @Override
    public void onClick(View view) {
        if (view == buttonPlay) {
            actionPlay=true;
            if(!serviceStartedM && !serviceBoundM){
                //start music and media player service
                serviceStartedM=true;
                Log.d(TAG, "Starting service");
                startService(i);

            }
            else{
                //start music
                playerService.startMusic(getApplication());
            }

        } else if (view == buttonPause) {
            //pause music
            playerService.pauseMusic();
        }else if (view == buttonStop) {
          //stop music
            playerService.stopMusic(getApplication());
        }
        else if (view == buttonExit) {
            if(serviceBoundM && serviceStartedM) {
                //exit application
                playerService.stopMusic(getApplication());
                Log.d(TAG, "Exiting service");
                playerService.stopAccService(getApplication());
                stopService(i);
                unbindService(mConnectionM);
            }
            System.exit(0);
        }
        //disable gesture commands
        else if (view == buttonGoff) {
            if(serviceStartedM && serviceBoundM) {
                playerService.stopAccService(getApplication());
            }
            else {
                Toast.makeText(getApplicationContext(), "Start the media player service",
                        Toast.LENGTH_LONG).show();
            }
        }
        //enable gesture commands
        else if (view == buttonGon) {
            if(serviceStartedM && serviceBoundM) {

                playerService.startAccService(getApplication());
            }
            else{
                Toast.makeText(getApplicationContext(), "Start the media player service",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
